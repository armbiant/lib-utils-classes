/**
 * Function that reduces object data by path.
 *
 * @param {string} path Specifies where to look for the sub-property of the object to find it in the data.
 * @param {object} obj Raw data.
 * @returns {object|array} Filtered data.
 */
const getSubProperty = <T>(path: string, obj: any): T => {
    if (path === "") {
        return ((obj as unknown) as T);
    } else {
        return (path.split(".").reduce((prev, curr) => {
            return (prev && prev[curr]) ? prev[curr] : undefined;
        }, obj) as T);
    }
};

export { getSubProperty };
