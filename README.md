# Golemio Utils Library

Library of Utils classes of the Golemio Data Platform System.

Developed by http://operatorict.cz

## Prerequisites

- node.js (https://nodejs.org)
- yarn (https://yarnpkg.com)
- TypeScript (https://www.typescriptlang.org/)

## Installation

Install Node

Install all npm modules using command:
```
yarn
```

## Compilation of typescript code

To compile typescript code into js one-time

```
yarn run build
```
or run this, to watch all changes
```
yarn run build-watch
```
from the application's root directory.


## Usage

In your project's `package.json` set dependency to Utils
```
npm install @golemio/utils --save

or

yarn add @golemio/utils --save
```

Then import module, e.g.
```
import * as ObjectUtils from "@golemio/utils";
```

## Documentation

For generating documentation run `yarn run generate-docs`. TypeDoc source code documentation is located in `docs/typedoc`.

## Contribution guidelines

Please read `CONTRIBUTING.md`.

## Troubleshooting

Contact vyvoj@operatorict.cz
